//
//  PokemonsViewModelTest.swift
//  PokedexAppTests
//
//  Created by cristian sarmiento on 4/4/20.
//  Copyright © 2020 cristian sarmiento. All rights reserved.
//

import XCTest
import RxSwift
import RxTest
import RxBlocking
@testable import PokedexApp

class PokemonsViewModelTest: XCTestCase {

    var viewModel : PokemonsViewModel!
    var scheduler: TestScheduler!
    var disposeBag: DisposeBag!
    
    fileprivate var service :  FakePokemonsService!
    
    override func setUp() {
        super.setUp()
        self.scheduler = TestScheduler(initialClock: 0)
        self.disposeBag = DisposeBag()
        self.service = FakePokemonsService()
        self.viewModel = PokemonsViewModel(service: service)
    }
    
    override func tearDown() {
        self.viewModel = nil
        self.service = nil
        super.tearDown()
    }
    
    func testFetchWithError () {
        
        let pokemons = scheduler.createObserver([PokemonModel].self)
        let errorMessage =  scheduler.createObserver(String.self)
        
        service.pokemons = nil
        
        viewModel.output.errorMessage
            .drive(errorMessage)
            .disposed(by: disposeBag)
        
        viewModel.output.pokemons
            .drive(pokemons)
            .disposed(by: disposeBag)
        
        scheduler.createColdObservable([.next(10, ())])
            .bind(to: viewModel.input.reload)
            .disposed(by: disposeBag)
        scheduler.start()
        
        XCTAssertEqual(errorMessage.events, [.next(10, "Error test")])
    }
    
    func testFetchWithNoPokemons () {
        
        let pokemons = scheduler.createObserver([PokemonModel].self)
        
        service.pokemons = nil
        
        viewModel.output.pokemons
            .drive(pokemons)
            .disposed(by: disposeBag)
        
        scheduler.createColdObservable([.next(10, ())])
            .bind(to: viewModel.input.reload)
            .disposed(by: disposeBag)
        scheduler.start()
        
        XCTAssertEqual(pokemons.events, [.next(10, []), .completed(10)])
    }
    
    func testFetchPokemons () {
        
        let pokemons = scheduler.createObserver([PokemonModel].self)
        
        let expectedPokemons = [PokemonModel(name: "test", url: "test.com")]
        service.pokemons =  expectedPokemons
        
        viewModel.output.pokemons
        .drive(pokemons)
        .disposed(by: disposeBag)
        
        // mock a reload
        scheduler.createColdObservable([.next(10, ()), .next(30, ())])
            .bind(to: viewModel.input.reload)
            .disposed(by: disposeBag)
        
        scheduler.start()
        
        XCTAssertEqual(pokemons.events, [.next(10, expectedPokemons), .next(30, expectedPokemons)])

    }
    
}

fileprivate class FakePokemonsService : PokemonsService {
    
    var pokemons:[PokemonModel]?
    
    override func getPokemonList(offSet: Int, size: Int) -> Observable<[PokemonModel]> {
        if let pokemons = self.pokemons {
            return  Observable.just(pokemons)
        } else {
            return Observable.error(ErrorResult.custom(string: "Error test"))
        }
    }
}
