//
//  PokemonDetaiilViewModelTest.swift
//  PokedexAppTests
//
//  Created by cristian sarmiento on 4/4/20.
//  Copyright © 2020 cristian sarmiento. All rights reserved.
//

import XCTest
import RxSwift
import RxTest
import RxBlocking
@testable import PokedexApp

class PokemonDetaiilViewModelTest: XCTestCase {
    
    var viewModel : PokemonDetailViewModel!
    var scheduler: TestScheduler!
    var disposeBag: DisposeBag!
    
    fileprivate var service :  FakePokemonDetailService!
    
    override func setUp() {
        super.setUp()
        self.scheduler = TestScheduler(initialClock: 0)
        self.disposeBag = DisposeBag()
        self.service = FakePokemonDetailService()
        self.viewModel = PokemonDetailViewModel("", service: service)
    }
    
    override func tearDown() {
        self.viewModel = nil
        self.service = nil
        super.tearDown()
    }
    
    func testFetchWithError () {
        
        let pokemon = scheduler.createObserver(PokemonDetailModel.self)
        let errorMessage =  scheduler.createObserver(String.self)
        
        service.pokemon = nil
        
        viewModel.output.errorMessage
            .drive(errorMessage)
            .disposed(by: disposeBag)
        
        viewModel.output.pokemon
            .drive(pokemon)
            .disposed(by: disposeBag)
        
        scheduler.createColdObservable([.next(10, ())])
            .bind(to: viewModel.input.reload)
            .disposed(by: disposeBag)
        scheduler.start()
        
        XCTAssertEqual(errorMessage.events, [.next(10, "Error test")])
    }
    
    func testFetchpokemon  () {
        
        let pokemon = scheduler.createObserver(PokemonDetailModel.self)
        
        let expectedpokemon = PokemonDetailModel(name:"Test")
        service.pokemon =  expectedpokemon
        
        viewModel.output.pokemon
            .drive(pokemon)
            .disposed(by: disposeBag)
        
        // mock a reload
        scheduler.createColdObservable([.next(10, ()), .next(30, ())])
            .bind(to: viewModel.input.reload)
            .disposed(by: disposeBag)
        
        scheduler.start()
        
        XCTAssertEqual(pokemon.events, [.next(10, expectedpokemon), .next(30, expectedpokemon)])
        
    }
    
}

fileprivate  class FakePokemonDetailService : PokemonDetailService {
    var pokemon:PokemonDetailModel?
    
    override func getPokemonDetail (_ name:String) -> Observable<PokemonDetailModel>  {
        if let pokemon = pokemon  {
            return Observable.just(pokemon)
        } else {
            return Observable.error(ErrorResult.custom(string: "Error test"))
        }
    }
}
