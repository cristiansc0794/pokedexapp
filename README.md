


## Prueba técnica Pokedex

### Arquitectura:
- MVVM

### Estructura:
- Model
- ViewModel
- View
- Wireframe
- Route

### Modulos:
- Pokemons
	- Lista de pokemons
- Detail
	- Detalle de un pokemon seleccionado
	
## Pruebas unitarias:
- Pruebas de ViewModel Pokemons
- Pruebas de ViewModel Detail