//
//  PokemonsViewModel.swift
//  PokedexApp
//
//  Created by cristian sarmiento on 4/4/20.
//  Copyright © 2020 cristian sarmiento. All rights reserved.
//

import RxSwift
import RxCocoa

class PokemonsViewModel {
    var input: Input!
    var output: Output!
    
    private let service: PokemonsService
    private let size = 20
    
    private var pokemonsVersion = [PokemonModel]()
    private var pokemonsVersionAux = [PokemonModel]()
    
    
    struct Input {
        let offSet: AnyObserver<Int>
        let query: AnyObserver<String>
        let reload: PublishRelay<Void>
    }
    
    struct Output {
        let pokemons: Driver<[PokemonModel]>
        let errorMessage: Driver<String>
    }
    
    init(_ page:Int = 0, service:PokemonsService = PokemonsService()) {
        self.service = service
        
        let errorRelay = PublishRelay<String>()
        
        let _offSet = BehaviorSubject<Int>(value: page)
        
        let _query = BehaviorSubject<String>(value: "")
        
        let _reloadRelay = PublishRelay<Void>()
        
        let pokemonList = Observable.combineLatest(_offSet, _query, _reloadRelay){ offSet, query, _ in
            (offSet, query) }
            .flatMapLatest{ (argument) -> Observable<[PokemonModel]> in
                let (page, query) = argument
                if query.isEmpty {
                    if self.pokemonsVersion.count != 0 && page == 0  {
                        self.pokemonsVersionAux = []
                        self.pokemonsVersion = []
                    }
                    return service.getPokemonList(offSet: page*self.size, size: self.size)
                } else {
                    if self.pokemonsVersion.count > self.pokemonsVersionAux.count {
                        self.pokemonsVersionAux = self.pokemonsVersion.map {$0}
                    }
                    self.pokemonsVersion = []
                    return self.filter(withQuery: query)
                }
        }.map { list in
            self.pokemonsVersion = self.pokemonsVersion + list
            print("version_count", self.pokemonsVersion.count)
            return self.pokemonsVersion
        }.asDriver {(error) -> Driver<[PokemonModel]> in
            errorRelay.accept((error as? ErrorResult)?.localizedDescription ?? error.localizedDescription)
            return Driver.just(self.pokemonsVersion)
        }
        
        self.input = Input(offSet: _offSet.asObserver(),
                           query: _query.asObserver(),
                           reload: _reloadRelay)
        
        self.output = Output(pokemons: pokemonList, errorMessage: errorRelay.asDriver(onErrorJustReturn: "An error happened"))
        
    }
    
    private func filter (withQuery query:String) -> Observable<[PokemonModel]> {
        return Observable<[PokemonModel]>.create { (observer) -> Disposable in
            let filtered = self.self.pokemonsVersionAux.filter { ($0.name?.lowercased().contains(query.lowercased())) ?? false
            }
            observer.onNext(filtered)
            return Disposables.create {}
        }
    }
    
}
