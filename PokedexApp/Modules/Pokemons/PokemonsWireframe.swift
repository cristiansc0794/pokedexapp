//
//  PokemonsWireframe.swift
//  PokedexApp
//
//  Created by cristian sarmiento on 4/4/20.
//  Copyright © 2020 cristian sarmiento. All rights reserved.
//

import UIKit

class PokemonsWireframe {
    static func setup () -> UIViewController {
        let vc = PokemonsViewController(nibName: "PokemonsViewController", bundle: nil)
        let viewModel = PokemonsViewModel()
        let route = PokemonsRoute(vc)
        vc.viewModel = viewModel
        vc.route = route
        return vc
    }
}


