//
//  PokemonsService.swift
//  PokedexApp
//
//  Created by cristian sarmiento on 4/4/20.
//  Copyright © 2020 cristian sarmiento. All rights reserved.
//

import RxSwift
import RxAlamofire
import Alamofire

class PokemonsService {
    final private let pathPokemonList = "pokemon"
    final private let disposeBag = DisposeBag()
    
    func getPokemonList (offSet:Int, size:Int) -> Observable<[PokemonModel]> {
        return Observable<[PokemonModel]>.create { (observer) -> Disposable in
            let parameters:[String:String] = [
                Constants.general.offset:String(offSet),
                Constants.general.limit:String(size)
            ]
            
            let url = Constants.general.serverUrl + self.pathPokemonList
            SessionManager.share.manager.rx .request(.get, url
                .addingPercentEncoding(withAllowedCharacters:NSCharacterSet.urlQueryAllowed)!, parameters: parameters as [String : Any])
                .responseJSON()
                .observeOn(MainScheduler.instance)
                .subscribe(onNext: { (response) in
                    switch response.result {
                    case .success(let data):
                        let result = ParserManager.parse(ResultListModel<Array<PokemonModel>>.self, withJSONobject: data)
                        switch result {
                        case .success(let object):
                            observer.onNext(object.results ?? [])
                            break
                        case .failure(let error):
                            observer.onError(error)
                            break
                        }
                        break
                    case .failure(let error):
                        observer.onError(error)
                        break
                    }
                }).disposed(by: self.disposeBag)
            
            return Disposables.create {
                
            }
        }
    }
    
}
