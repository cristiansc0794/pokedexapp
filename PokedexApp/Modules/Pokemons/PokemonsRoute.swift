//
//  PokemonsRoute.swift
//  PokedexApp
//
//  Created by cristian sarmiento on 4/4/20.
//  Copyright © 2020 cristian sarmiento. All rights reserved.
//

import Foundation

class  PokemonsRoute {
    let view:PokemonsViewController
    
    init(_ view:PokemonsViewController) {
        self.view = view
    }
    
    func navigateToDetail (pokemonName:String) {
        view.navigationController?.pushViewController(PokemonDetailWirefame.setup(name: pokemonName), animated: true)
    }
}
