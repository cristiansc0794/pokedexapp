//
//  PokemonsViewController.swift
//  PokedexApp
//
//  Created by cristian sarmiento on 4/4/20.
//  Copyright © 2020 cristian sarmiento. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift

class PokemonsViewController: UIViewController {
    
    @IBOutlet weak var tableViewPokemon: UITableView! {
        didSet {
            tableViewPokemon.register(PokemonTableViewCell.getNib(),
                                      forCellReuseIdentifier: PokemonTableViewCell.identifier)
            tableViewPokemon.delegate = self
        }
    }
    @IBOutlet weak var emptyStateLabel: UILabel! {
        didSet {
            emptyStateLabel.isHidden = true
        }
    }
    
    //MARK: - Constants
    private let disposeBag = DisposeBag()
    private let refreshControl = UIRefreshControl()
    
    //MARK: - Variables
    var route:PokemonsRoute!
    var viewModel:PokemonsViewModel!
    var page : Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = Constants.pokemonList.title
        self.addSearchBar()
        refreshControl.sendActions(for: .valueChanged)
        tableViewPokemon.addSubview(refreshControl)
        binding()
        self.viewModel.input.reload.accept(())
        // Do any additional setup after loading the view.
    }
    
    // MARK: - Private methods
    
    private func addSearchBar () {
        let searchController = UISearchController(searchResultsController: nil)
        //        searchController.searchResultsUpdater = self.viewModel
        searchController.obscuresBackgroundDuringPresentation = false
        searchController.searchBar.placeholder = Constants.pokemonList.searchPlaceholder
        searchController.searchBar.tintColor = UIColor.gray
        searchController.searchBar.barTintColor = UIColor.gray
        searchController.searchBar.delegate = self
        self.navigationItem.searchController = searchController
        self.definesPresentationContext = true
        self.navigationItem.hidesSearchBarWhenScrolling = false
        searchController.searchBar.rx.text.orEmpty
            .throttle(.milliseconds(300), scheduler: MainScheduler.instance).distinctUntilChanged().skip(1).bind {query in
                self.viewModel.input.query.onNext(query)
        }.disposed(by: disposeBag)
    }
    
}

// MARK: - Binding ViewModel
extension PokemonsViewController  {
    func binding () {
        
        self.viewModel.output.errorMessage
            .drive(onNext: { errorMessage in
                AlertView.show(withTitle: "Error inesperado :(", body: errorMessage, viewController: self)
            }).disposed(by: disposeBag)
        
        self.tableViewPokemon.rx
            .willDisplayCell
            .subscribe(onNext: { cellInfo in
                let (_, indexPath) = cellInfo
                let rowsAmount = self.tableViewPokemon.numberOfRows(inSection: indexPath.section)
                if rowsAmount >= 20 && indexPath.row == rowsAmount - 3 {
                    self.page += 1
                    self.viewModel.input.offSet.onNext(self.page)
                }
            })
            .disposed(by: disposeBag)
        
        self.viewModel.output.pokemons
            .do(onNext: { [weak self] _ in
                self?.refreshControl.endRefreshing()})
            .drive(self.tableViewPokemon.rx.items(cellIdentifier: PokemonTableViewCell.identifier, cellType: PokemonTableViewCell.self)) {(row, pokemon, cell) in
                cell.setPokemonId(pokemon.getIdPokemon() ?? "")
                cell.labelName.text = pokemon.name
        }.disposed(by: disposeBag)
        
        self.tableViewPokemon.rx.itemSelected.map { indexPath in
            return self.tableViewPokemon.cellForRow(at: indexPath)
        }.subscribe(onNext:{cell in
            if let pokemonCell = cell as? PokemonTableViewCell  {
                let viewBackground = UIView()
                let gradientView = GradientView(colors: [UIColor.PokeColor.blue, UIColor.PokeColor.green], startPoint: .topLeft, endPoint: .topRight, locations: [0,1])
                viewBackground.addSubview(gradientView)
                gradientView.setupConstraints()
                pokemonCell.selectedBackgroundView = viewBackground
                self.route.navigateToDetail(pokemonName: pokemonCell.labelName.text ?? "")
            }
            })
            .disposed(by: disposeBag)
        
        self.viewModel.output.pokemons
            .map{$0.count != 0}
            .drive(self.emptyStateLabel.rx.isHidden)
            .disposed(by: disposeBag)
        
        refreshControl.rx.controlEvent(.valueChanged)
            .do(onNext: { [weak self] _ in
                self?.page = 0
                self?.viewModel.input.offSet.onNext(0)
            }).bind(to: viewModel.input.reload)
            .disposed(by: disposeBag)
    }
}

// MARK: - UISearchBarDelegate
extension  PokemonsViewController : UISearchBarDelegate {
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        self.view.endEditing(true)
    }
}
// MARK: - UITableViewDelegate
extension PokemonsViewController : UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }
}
