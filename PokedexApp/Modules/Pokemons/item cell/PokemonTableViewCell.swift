//
//  PokemonTableViewCell.swift
//  PokedexApp
//
//  Created by cristian sarmiento on 4/4/20.
//  Copyright © 2020 cristian sarmiento. All rights reserved.
//

import UIKit
import SDWebImage

class PokemonTableViewCell: UITableViewCell {

    static let identifier = "PokemonTableViewCell"
    
    @IBOutlet weak var labelName: UILabel!
    @IBOutlet weak var imagePokemon: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    static func getNib () ->UINib {
        let nib = UINib(nibName: PokemonTableViewCell.identifier, bundle: nil)
        return nib
    }
    
    func setPokemonId (_ id:String) {
        let  urlString = String(format: Constants.pokemonList.urlImageDefault, id)
        imagePokemon.sd_setImage(with: URL(string: urlString))
    }

}
