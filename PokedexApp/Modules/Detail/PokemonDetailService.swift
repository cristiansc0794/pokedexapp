//
//  PokemonDetailService.swift
//  PokedexApp
//
//  Created by cristian sarmiento on 4/4/20.
//  Copyright © 2020 cristian sarmiento. All rights reserved.
//

import RxSwift
import RxAlamofire

class PokemonDetailService {
    let disposeBag = DisposeBag()
    let pathDetail = "pokemon/%@"
    
    func getPokemonDetail (_ name:String) -> Observable<PokemonDetailModel> {
        return Observable<PokemonDetailModel>.create { (observer) -> Disposable in
            
            var url = Constants.general.serverUrl + self.pathDetail
            url = String(format: url, name)
            SessionManager.share.manager.rx .request(.get,url
                .addingPercentEncoding(withAllowedCharacters:NSCharacterSet.urlQueryAllowed)!, parameters: [:])
                .responseJSON()
                .observeOn(MainScheduler.instance)
                .subscribe(onNext: { (response) in
                    switch response.result {
                    case .success(let data):
                        let result = ParserManager.parse(PokemonDetailModel.self, withJSONobject: data)
                        switch result {
                        case .success(let object):
                            observer.onNext(object)
                            break
                        case .failure(let error):
                            observer.onError(error)
                            break
                        }
                        break
                    case .failure(let error):
                        observer.onError(error)
                        break
                    }
                }).disposed(by: self.disposeBag)
            
            return Disposables.create {}
        }
    }
}
