//
//  PokemonDetailViewModel.swift
//  PokedexApp
//
//  Created by cristian sarmiento on 4/4/20.
//  Copyright © 2020 cristian sarmiento. All rights reserved.
//

import RxSwift
import RxCocoa


class PokemonDetailViewModel {
 
    private let service:PokemonDetailService
    
    
    let output: Output
    let input:Input
    
    struct Input {
        let reload:PublishRelay<Void>
    }
    
    struct Output {
        let pokemon:Driver<PokemonDetailModel>
        let errorMessage: Driver<String>
    }
    
    init(_ pokemonName:String, service:PokemonDetailService = PokemonDetailService()) {
        self.service = service
        let errorRelay = PublishRelay<String>()
        let _reloadRelay = PublishRelay<Void>()
        
        let pokemon = _reloadRelay.asObservable()
            .flatMapLatest({service.getPokemonDetail(pokemonName)})
            .map { pokemonResp in
                return pokemonResp
        }.asDriver {(error) -> Driver<PokemonDetailModel> in
            errorRelay.accept((error as? ErrorResult)?.localizedDescription ?? error.localizedDescription)
            return Driver.just(PokemonDetailModel(name:pokemonName))
        }
        
        self.input = Input(reload: _reloadRelay)
        self.output = Output(pokemon: pokemon,
                             errorMessage: errorRelay
                                .asDriver(onErrorJustReturn: "An error happened"))
    }
    
}
