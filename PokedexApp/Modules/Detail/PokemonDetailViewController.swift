//
//  PokemonDetailViewController.swift
//  PokedexApp
//
//  Created by cristian sarmiento on 4/4/20.
//  Copyright © 2020 cristian sarmiento. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift

class PokemonDetailViewController: UIViewController {

    @IBOutlet weak var labelStats: UILabel!
    @IBOutlet weak var viewType: UIView!
    @IBOutlet weak var imageViewType: UIImageView!
    @IBOutlet weak var labelType: UILabel!
    @IBOutlet weak var labelName: UILabel!
    @IBOutlet weak var collectionViewSprites: UICollectionView! {
        didSet {
            self.collectionViewSprites.register(SpriteCollectionViewCell.geNib(), forCellWithReuseIdentifier: SpriteCollectionViewCell.identifier)
        }
    }
    @IBOutlet weak var tableViewStats: UITableView!  {
        didSet {
            self.tableViewStats.register(StatTableViewCell.getNib(), forCellReuseIdentifier: StatTableViewCell.identifier)
        }
    }
    
    private let disposeBag = DisposeBag()
    private let statsRelay = BehaviorRelay<Array<StatBaseModel>>(value: [])
    private let spritesRelay = BehaviorRelay<Array<String>>(value: [])
    
    var viewModel:PokemonDetailViewModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.binding()
        self.viewModel.input.reload.accept(())
        // Do any additional setup after loading the view.
    }
    
    override func viewDidLayoutSubviews() {
        super.viewWillLayoutSubviews()
           
           let frame = self.navigationController?.navigationBar.frame

           
           let height: CGFloat = 200
           let bounds = self.navigationController!.navigationBar.bounds
           self.navigationController?.navigationBar.frame = CGRect(x: (frame?.origin.x)!, y: (frame?.origin.y)!, width: bounds.width, height: bounds.height + height)
    }


}


// MARK: - Binding ViewModel
extension PokemonDetailViewController {
    func  binding () {
        self.viewModel.output.errorMessage
        .drive(onNext: { errorMessage in
            AlertView.show(withTitle: "Error inesperado :(", body: errorMessage, viewController: self)
        }).disposed(by: disposeBag)
        
        self.viewModel.output.pokemon
        .map {$0.name}
        .drive(self.labelName.rx.text)
        .disposed(by: disposeBag)
        
        self.viewModel.output.pokemon
            .map {$0.types?.first?.type?.name}
            .drive(self.labelType.rx.text)
            .disposed(by: disposeBag)
        
        self.viewModel.output.pokemon
            .map {$0.types?.first?.type?.iconAndColorType().0}
            .drive(self.imageViewType.rx.image)
            .disposed(by: disposeBag)
        
        self.viewModel.output.pokemon
            .map{$0.types?.first?.type?.iconAndColorType().1}
            .drive(self.viewType.rx.backgroundColor)
            .disposed(by: disposeBag)
        
        self.viewModel.output.pokemon
            .map{$0.types?.first?.type?.iconAndColorType().1}
            .drive(self.labelStats.rx.backgroundColor)
            .disposed(by: disposeBag)
        
        self.viewModel.output.pokemon
            .map {
                let color  = $0.types?.first?.type?.iconAndColorType().1
                var stats = [StatBaseModel]()
                ($0.stats ?? []).forEach { val in
                    var val = val
                    val.color = color
                    stats.append(val)
                }
                return stats
            }
            .drive(self.statsRelay)
            .disposed(by: disposeBag)
        
        self.viewModel.output.pokemon.map {
            ($0.sprites?.getArraySprites() ?? [])
            }.drive(self.spritesRelay)
            .disposed(by: disposeBag)
        
        self.statsRelay.asDriver().drive(self.tableViewStats.rx.items(cellIdentifier: StatTableViewCell.identifier, cellType: StatTableViewCell.self)) {(row, stat, cell) in
            cell.setData(name: stat.stat?.abbreviatedName() ?? "", points: stat.baseStat ?? 0, color: stat.color)
        }.disposed(by: disposeBag)
        
        self.spritesRelay.asDriver().drive(self.collectionViewSprites.rx.items(cellIdentifier: SpriteCollectionViewCell.identifier, cellType: SpriteCollectionViewCell.self)) {(row, sprite, cell) in
            cell.setUrlSprite(sprite)
        }.disposed(by: disposeBag)
        
    }
}
