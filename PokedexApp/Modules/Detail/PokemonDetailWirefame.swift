//
//  PokemonDetailWirefame.swift
//  PokedexApp
//
//  Created by cristian sarmiento on 4/4/20.
//  Copyright © 2020 cristian sarmiento. All rights reserved.
//

import UIKit

class PokemonDetailWirefame {
    static func setup (name:String) -> UIViewController {
        let vc = PokemonDetailViewController(nibName: "PokemonDetailViewController", bundle: nil)
        let viewModel = PokemonDetailViewModel(name)
        vc.viewModel = viewModel
        return  vc
    }
}
