//
//  StatTableViewCell.swift
//  PokedexApp
//
//  Created by cristian sarmiento on 4/4/20.
//  Copyright © 2020 cristian sarmiento. All rights reserved.
//

import UIKit

class StatTableViewCell: UITableViewCell {

    
    static let identifier  = "StatTableViewCell"
    
    private let totalPoint:Float = 100
    
    @IBOutlet weak var labelName: UILabel!
    @IBOutlet weak var labelPoints: UILabel!
    @IBOutlet weak var progressBar: UIProgressView! {
        didSet {
            progressBar.transform = progressBar.transform.scaledBy(x: 1, y: 3)
            progressBar.clipsToBounds = true
            progressBar.layer.cornerRadius = 4
        }
    }
    
    
    static func getNib () -> UINib {
        return UINib(nibName: StatTableViewCell.identifier, bundle: nil)
    }
    
    
    func setData (name:String, points:Int, color:UIColor?) {
        self.labelName.text = name
        self.labelName.textColor = color ?? UIColor.darkGray
        self.labelPoints.text = String(points)
        let value = Float(points)/totalPoint
        self.progressBar.progress = value
        self.progressBar.progressTintColor = color
    }
    
}
