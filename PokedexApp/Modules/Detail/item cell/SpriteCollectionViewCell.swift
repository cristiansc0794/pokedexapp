//
//  SpriteCollectionViewCell.swift
//  PokedexApp
//
//  Created by cristian sarmiento on 4/4/20.
//  Copyright © 2020 cristian sarmiento. All rights reserved.
//

import UIKit
import SDWebImage

class SpriteCollectionViewCell: UICollectionViewCell {
    
    static let identifier = "SpriteCollectionViewCell"
    
    @IBOutlet weak var imageView: UIImageView!
    
    
    static func geNib () -> UINib {
        return UINib(nibName: SpriteCollectionViewCell.identifier, bundle: nil)
    }
    
    func setUrlSprite (_ urlString:String) {
        imageView.sd_setImage(with: URL(string: urlString))
    }
    
}
