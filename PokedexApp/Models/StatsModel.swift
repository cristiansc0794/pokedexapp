//
//  StatModel.swift
//  PokedexApp
//
//  Created by cristian sarmiento on 4/4/20.
//  Copyright © 2020 cristian sarmiento. All rights reserved.
//
import UIKit

struct StatModel : Codable {
    let name:String?
    let url:String?
    
    func abbreviatedName () -> String {
        switch self.name {
        case "speed":
            return "SPD"
        case "special-defense":
            return "SDEF"
        case "special-attack":
            return "SATK"
        case "defense":
            return "DEF"
        case "attack":
            return "ATK"
        case "hp":
            return "HP"
        default:
            return "NN"
        }
    }
    
}

struct StatBaseModel : Codable {
    let baseStat:Int?
    let stat:StatModel?
    var color:UIColor? 
    
    enum CodingKeys: String, CodingKey {
        case baseStat = "base_stat"
        case stat
    }
}



