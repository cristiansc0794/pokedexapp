//
//  SpritesModel.swift
//  PokedexApp
//
//  Created by cristian sarmiento on 4/4/20.
//  Copyright © 2020 cristian sarmiento. All rights reserved.
//

import Foundation

struct SpritesModel : Codable {
    let backDefault:String?
    let backFemale:String?
    let backShiny:String?
    let backShinyFemale:String?
    let frontDefault:String?
    let frontFemale:String?
    let frontShiny:String?
    let frontShinyFemale:String?
    
    enum CodingKeys: String, CodingKey {
        case backDefault = "back_default"
        case backFemale = "back_female"
        case backShiny = "back_shiny"
        case backShinyFemale = "back_shiny_female"
        case frontDefault = "front_default"
        case frontFemale = "front_female"
        case frontShiny = "front_shiny"
        case frontShinyFemale = "front_shiny_female"
    }
    
    func getArraySprites ()  -> [String] {
        var arr = [String]()
        if let backDefault = backDefault {
            arr.append(backDefault)
        }
        if let backFemale = backFemale {
            arr.append(backFemale)
        }
        if let backShiny = backShiny {
            arr.append(backShiny)
        }
        if let backShinyFemale = backShinyFemale {
            arr.append(backShinyFemale)
        }
        if let frontDefault =  frontDefault {
            arr.append(frontDefault)
        }
        if let frontFemale = frontFemale {
            arr.append(frontFemale)
        }
        if let frontShiny = frontShiny {
            arr.append(frontShiny)
        }
        if let frontShinyFemale = frontShinyFemale {
            arr.append(frontShinyFemale)
        }
        return arr
    }
    
}
