//
//  PokemonModel.swift
//  PokedexApp
//
//  Created by cristian sarmiento on 4/4/20.
//  Copyright © 2020 cristian sarmiento. All rights reserved.
//

import Foundation


struct PokemonModel : Codable {
    let name:String?
    let url:String?
    
    func getIdPokemon ()  -> String?{
        
        if let url  = self.url, url.count > 34 {
            let character = url.split(separator: "/").last
            return String(character ?? "0")
        }
        
        return nil
    }
}

extension PokemonModel : Equatable {
    static func == (lhs: PokemonModel, rhs: PokemonModel) -> Bool {
        return  lhs.name == rhs.name
    }
}
