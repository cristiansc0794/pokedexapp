//
//  PokemonDetailModel.swift
//  PokedexApp
//
//  Created by cristian sarmiento on 4/4/20.
//  Copyright © 2020 cristian sarmiento. All rights reserved.
//

import Foundation

struct PokemonDetailModel : Codable {
    let id:Int?
    let name:String?
    let sprites:SpritesModel?
    let stats:Array<StatBaseModel>?
    let types:Array<PokemonTypeModel>?
    
    init( id:Int? = nil,
          name:String? = nil,
          sprites:SpritesModel? = nil,
          stats:Array<StatBaseModel>? = nil,
          types:Array<PokemonTypeModel>? = nil ) {
        self.id = id
        self.name = name
        self.sprites =  sprites
        self.stats = stats
        self.types = types
    }
    
}

extension PokemonDetailModel : Equatable {
    static func == (lhs: PokemonDetailModel, rhs: PokemonDetailModel) -> Bool {
        return lhs.name == rhs.name
    }

}
