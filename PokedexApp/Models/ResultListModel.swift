//
//  ResultListModel.swift
//  PokedexApp
//
//  Created by cristian sarmiento on 4/4/20.
//  Copyright © 2020 cristian sarmiento. All rights reserved.
//

struct ResultListModel<T:Codable> : Codable {
    let count:Int?
    let next:String?
    let previous:String?
    let results:T?
}


