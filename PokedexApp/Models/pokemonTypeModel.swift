//
//  pokemonTypeModel.swift
//  PokedexApp
//
//  Created by cristian sarmiento on 4/4/20.
//  Copyright © 2020 cristian sarmiento. All rights reserved.
//

import UIKit

struct PokemonTypeModel : Codable {
    let slot:Int?
    let type:TypeModel?
}

struct TypeModel : Codable {
    let name:String?
    let url:String?
    
    func iconAndColorType () -> (UIImage?, UIColor) {
        switch self.name {
        case "bug":
            return (UIImage(named: "icon_bug"), UIColor.PokeColor.colorBug)
        case "dark":
            return (UIImage(named: "icon_dark"), UIColor.PokeColor.colorDark)
        case "dragon":
            return (UIImage(named: "icon_dragon"), UIColor.PokeColor.colorDragon)
        case "electric":
            return (UIImage(named: "icon_electric"), UIColor.PokeColor.colorElectric)
        case "fairy":
            return (UIImage(named: "icon_fairy"), UIColor.PokeColor.colorFairy)
        case "fight":
            return (UIImage(named: "icon_fight"), UIColor.PokeColor.colorFight)
        case "fire":
            return (UIImage(named: "icon_fire"), UIColor.PokeColor.colorFire)
        case "flying":
            return (UIImage(named: "icon_flying"), UIColor.PokeColor.colorFight)
        case "ghost":
            return (UIImage(named: "icon_ghost"), UIColor.PokeColor.colorGhost)
        case "grass":
            return (UIImage(named: "icon_grass"), UIColor.PokeColor.colorGrass)
        case "ground":
            return (UIImage(named: "icon_ground"), UIColor.PokeColor.colorGround)
        case "ice":
            return (UIImage(named: "icon_ice"), UIColor.PokeColor.colorIce)
        case "poison":
            return (UIImage(named: "icon_poison"), UIColor.PokeColor.colorPoison)
        case "psychic":
            return (UIImage(named: "icon_psychic"), UIColor.PokeColor.colorPsychic)
        case "rock":
            return (UIImage(named: "icon_rock"), UIColor.PokeColor.colorRock)
        case "steel":
            return (UIImage(named: "icon_steel"), UIColor.PokeColor.colorSteel)
        case "water":
            return (UIImage(named: "icon_water"), UIColor.PokeColor.colorWater)
        default:
            return (UIImage(named: "icon_normal"), UIColor.PokeColor.colorNormal)
        }
    }
}
