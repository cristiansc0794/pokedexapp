//
//  ParserManager.swift
//  PokedexApp
//
//  Created by cristian sarmiento on 4/4/20.
//  Copyright © 2020 cristian sarmiento. All rights reserved.
//

import Foundation

class ParserManager {
    static func parse<T:Codable> (_ type:T.Type, withJSONobject:Any) -> Result<T, ErrorResult> {
        do {
            let data = try JSONSerialization.data(withJSONObject: withJSONobject, options: JSONSerialization.WritingOptions.prettyPrinted)
            let object = try JSONDecoder().decode(type, from: data)
            return Result.success(object)
        } catch {
            return Result.failure(ErrorResult.parser(string: error.localizedDescription))
        }
    }
}
