//
//  Constants.swift
//  PokedexApp
//
//  Created by cristian sarmiento on 4/4/20.
//  Copyright © 2020 cristian sarmiento. All rights reserved.
//

import Foundation


struct Constants {
    struct general {
        static let serverUrl = "https://pokeapi.co/api/v2/"
        static let offset = "offset"
        static let limit = "limit"
    }
    
    struct pokemonList {
        static let title = "Pokemon"
        static let searchPlaceholder = "Buscar Pokemon"
        static let urlImageDefault = "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/%@.png"
    }
}
