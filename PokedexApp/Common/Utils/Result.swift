//
//  Result.swift
//  PokedexApp
//
//  Created by cristian sarmiento on 4/4/20.
//  Copyright © 2020 cristian sarmiento. All rights reserved.
//

import Foundation


enum Result<T, E: Error> {
    case success(T)
    case failure(E)
}
