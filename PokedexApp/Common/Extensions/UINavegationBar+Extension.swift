//
//  UINavegationBar+Extension.swift
//  PokedexApp
//
//  Created by cristian sarmiento on 4/4/20.
//  Copyright © 2020 cristian sarmiento. All rights reserved.
//

import UIKit

extension UINavigationBar {
    func setGradientBackground(colors: [UIColor],
                               startPoint: GradientView.Point = .topLeft,
                               endPoint: GradientView.Point = .bottomLeft,
                               locations: [NSNumber] = [0, 1]) {
        guard let backgroundView = value(forKey: "backgroundView") as? UIView else { return }
        guard let gradientView = backgroundView.subviews.first(where: { $0 is GradientView }) as? GradientView else {
            let gradientView = GradientView(colors: colors, startPoint: startPoint,
                                                           endPoint: endPoint, locations: locations)
            backgroundView.addSubview(gradientView)
            gradientView.setupConstraints()
            return
        }
        gradientView.set(colors: colors, startPoint: startPoint, endPoint: endPoint, locations: locations)
    }
}
