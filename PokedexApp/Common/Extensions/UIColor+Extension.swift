//
//  UIColor+Extension.swift
//  PokedexApp
//
//  Created by cristian sarmiento on 4/4/20.
//  Copyright © 2020 cristian sarmiento. All rights reserved.
//

import UIKit


extension UIColor {
    struct PokeColor {
        static let blue = UIColor(red:0.87, green:0.91, blue:0.98, alpha:1.00)
        static let green = UIColor(red:0.86, green:0.96, blue:0.86, alpha:1.00)
        static let colorBug = UIColor(red:0.57, green:0.74, blue:0.17, alpha:1.00)
        static let colorDark = UIColor(red:0.35, green:0.34, blue:0.38, alpha:1.00)
        static let colorDragon = UIColor(red:0.05, green:0.41, blue:0.78, alpha:1.00)
        static let colorElectric = UIColor(red:0.93, green:0.84, blue:0.24, alpha:1.00)
        static let colorFairy = UIColor(red:0.93, green:0.55, blue:0.90, alpha:1.00)
        static let colorFight = UIColor(red:0.81, green:0.26, blue:0.40, alpha:1.00)
        static let colorFire = UIColor(red:0.98, green:0.61, blue:0.32, alpha:1.00)
        static let colorGhost = UIColor(red:0.32, green:0.42, blue:0.67, alpha:1.00)
        static let colorGrass = UIColor(red:0.37, green:0.74, blue:0.32, alpha:1.00)
        static let colorGround = UIColor(red:0.86, green:0.46, blue:0.27, alpha:1.00)
        static let colorIce = UIColor(red:0.44, green:0.80, blue:0.74, alpha:1.00)
        static let colorNormal = UIColor(red:0.57, green:0.60, blue:0.64, alpha:1.00)
        static let colorPoison = UIColor(red:0.66, green:0.39, blue:0.78, alpha:1.00)
        static let colorPsychic = UIColor(red:0.96, green:0.44, blue:0.44, alpha:1.00)
        static let colorRock = UIColor(red:0.77, green:0.71, blue:0.54, alpha:1.00)
        static let colorSteel = UIColor(red:0.32, green:0.53, blue:0.62, alpha:1.00)
        static let colorWater = UIColor(red:0.33, green:0.62, blue:0.87, alpha:1.00)
    }
}


