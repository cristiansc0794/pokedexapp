//
//  AlertView.swift
//  PokedexApp
//
//  Created by cristian sarmiento on 4/4/20.
//  Copyright © 2020 cristian sarmiento. All rights reserved.
//


import UIKit

class AlertView {
    static func show (withTitle title:String, body:String, viewController:UIViewController) {
        let alert = UIAlertController(title: title, message: body, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "Aceptar",
                                      style: UIAlertAction.Style.default,
                                      handler: { _ in
            alert.dismiss(animated: true, completion: nil)
        }))
        viewController.present(alert, animated: true, completion: nil)
        
    }
}
